import WC from 'wc-util'
/*
  - catches the "submit" event of the closest form
  - finds the ts-loadable (DOM ID specified in @for attribute) and
      * sets it's @href to the action and form parameters in this form
      * removes any possible extra_param "paginate"
      * forces load().

  for - the DOM ID of the ts-loadable that will get manipulated
*/

class TsLoadableForm extends HTMLElement {

  connectedCallback(){
    this.for = this.getAttribute("for")
    this.form = this.closest("form")
    this.form.addEventListener("submit", _form_submit.bind(this))
    _warn_about_get_parameters.bind(this)()
  }

  submit(){
    // without cancelable Firefox does a pageflip when submitting
    this.closest("form").dispatchEvent(new Event("submit", {bubbles: true, cancelable: true}))
  }

};

customElements.define('ts-loadable-form', TsLoadableForm)

function _warn_about_get_parameters(){
  if (console && this.form.action.includes("?")){
    console.warn("Form has GET parameters in it's action attribute. Those GET parameters will be ignored during SUBMIT!")
  }
}

function _form_submit(event){
  event.preventDefault()
  event.stopPropagation() /* otherwise we still get the double-submit from UJS */
  let ts_loadable = document.getElementById(this.for)
  if (ts_loadable){
    if (ts_loadable.extra_params){ delete(ts_loadable.extra_params.paginate) }
    ts_loadable.setAttribute(
      'href',
      this.form.action.split("?")[0] + /* dropping GET params, since they might be here due to pushState */
        "?" + WC.serialize_as_string(this.form)
    )
    ts_loadable.load()
  }
}
