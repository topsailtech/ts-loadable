import WC from 'wc-util'

/*
  This uses JQuery!

  attributes:   href
                insert (optional) - "top" or "bottom", inserts resulting content rather than
                                    replace the entire element content
                extra-params (optional) - JSON of initial extra_params
                lazyload (optional)     - don't immediately load the content

  properties:  extra_params - a plain object passed in to subsequent AJAX load requests;
                              when setting, you can provide a plain object or a "railsy" request string (the setter converts the string to an object)

  methods: load() - forces the element to fetch new content via AJAX, using it's @href and #extra_params
           update_extra_params(param_name, value) - sets the value of existing extra_params'
                                                    key identified by param_name (key
                                                    can be complex like "filter[id][]")

  events: ts-load - fired before loading. Can be cancelled
          ts-loaded - fired once the content is updated after a load()

  interactivity cues: ts-loadable.loading - class appended during load(); cleared when content is updated

*/


class TsLoadable extends HTMLElement {

  connectedCallback(){
    // set initial extra_params
    this.extra_params = this.hasAttribute("extra-params") ?
                            JSON.parse(this.getAttribute("extra-params")) :
                            {};

    // load on startup unless told otherwise
    if (!this.hasAttribute("lazyload")){ this.load(); }
  }

  get extra_params(){
    return this._extra_params;
  }

  set extra_params(string_or_object){ // url_request params or hash/object
    this._extra_params = typeof string_or_object == "string" ?
                                    WC.serialize_as_object(string_or_object) :
                                    string_or_object;
  }

  update_extra_params(param_name, value){
    if (!(typeof this._extra_params == "object")) {
      this._extra_params = {};
    }
    WC.railsy_update_property_hash(this._extra_params, param_name, value);
  }

  load(force){
    var el = this;

    // allow listeners to prevent variant change
    if ( !force && !WC.fireEvent(this, "ts-load") ){ return false; }

    el.classList.add("loading");
    el._current_jqxhr && el._current_jqxhr.abort(); // kill possible pending request

    this._current_jqxhr = WC.$get(this.getAttribute('href'), this.extra_params, function(responseText, textStatus, jqXHR){

      el._current_jqxhr = null;

      if (el.getAttribute("insert") == "top") {
        WC.prepend(el, responseText);
      } else if (el.getAttribute("insert") == "bottom") {
        WC.append(el, responseText);
      } else {
        WC.html(el, responseText); // use jquery instead of innerHTML so that JS gets executed
      }

      WC.fireEvent(el, "ts-loaded");
      el.classList.remove("loading");
    });
  }
};

customElements.define('ts-loadable', TsLoadable);